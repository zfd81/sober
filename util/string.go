package util

import (
	"github.com/spf13/cast"
	"github.com/zfd81/rooster/types/container"
)

func Fields(str string) []string {
	set := container.HashSet{}
	stack := container.NewArrayStack()
	strLen := len(str) //字符串长度
	start := -1        //替换内容的开始位置
	for i := 0; i < strLen; i++ {
		char := str[i]
		if (char >= 65 && char <= 90) || (char >= 97 && char <= 122) || char == 95 {
			if start == -1 && stack.Empty() {
				start = i
			}
		} else if char >= 48 && char <= 57 {
			continue
		} else {
			if stack.Empty() {
				if char == '\'' || char == '"' {
					stack.Push(char)
				} else {
					if start >= 0 {
						set.Add(str[start:i])
						start = -1
					}
				}
			} else {
				if char == '\'' || char == '"' {
					c, _ := stack.Peek()
					if c == char {
						stack.Pop()
					} else {
						stack.Push(char)
					}
				}
			}
		}
	}
	if start >= 0 && stack.Empty() {
		set.Add(str[start:strLen])
	}
	fields := make([]string, set.Size())
	set.Iterator(func(i int, v interface{}) {
		fields[i] = cast.ToString(v)
	})
	return fields
}
