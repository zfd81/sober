package store

import (
	"context"
	"fmt"
	"strings"
	"time"

	"gitee.com/zfd81/sober/config"
	"github.com/go-redis/redis/v8"
	"github.com/spf13/cast"
)

var (
	ctx = context.Background()
)

type Redis struct {
	client redis.Cmdable
}

func (db *Redis) Set(oid string, data map[string]interface{}) (int64, error) {
	return db.client.HSet(ctx, key(oid), data).Result()
}

func (db *Redis) Delete(oid string, attrs ...string) (int64, error) {
	return db.client.HDel(ctx, key(oid), attrs...).Result()
}

func (db *Redis) Clear(oid string) (int64, error) {
	return db.client.Del(ctx, key(oid)).Result()
}

func (db *Redis) Get(oid string, attrs ...string) (map[string]interface{}, error) {
	result := map[string]interface{}{}
	if len(attrs) > 0 {
		vals, err := db.client.HMGet(ctx, key(oid), attrs...).Result()
		if err != nil {
			return result, err
		}
		for i, attr := range attrs {
			result[attr] = vals[i]
		}
	} else {
		kvs, err := db.client.HGetAll(ctx, key(oid)).Result()
		if err != nil {
			return result, err
		}
		for k, v := range kvs {
			result[k] = v
		}
	}
	return result, nil
}

func (db *Redis) AttrSet(oid string) ([]string, error) {
	return db.client.HKeys(ctx, key(oid)).Result()
}

func (db *Redis) Count() (int64, error) {
	var keys []string
	var cursor uint64
	var err error
	cnt := 0
	for {
		keys, cursor, err = db.client.Scan(ctx, cursor, fmt.Sprintf("%s*", prefix()), 10000).Result()
		if err != nil {
			return -1, err
		}
		cnt += len(keys)
		if cursor == 0 {
			break
		}
	}
	return int64(cnt), nil
}

func (db *Redis) BatchSet(data []*object) error {
	_, err := db.client.Pipelined(ctx, func(pipe redis.Pipeliner) error {
		for _, v := range data {
			pipe.HSet(ctx, key(v.Id()), v.Values())
		}
		return nil
	})
	return err
}

func NewRedis(s *config.Store) *Redis {
	var rdb redis.Cmdable
	endpoints := strings.Split(s.Address, ",")
	if len(endpoints) == 1 {
		opts := &redis.Options{
			Addr:     endpoints[0],
			Password: s.Password,
			DB:       cast.ToInt(s.Database),
		}
		if s.DialTimeout > 0 {
			opts.DialTimeout = s.DialTimeout * time.Second
		}
		if s.ReadTimeout > 0 {
			opts.ReadTimeout = s.ReadTimeout * time.Second
		}
		if s.WriteTimeout > 0 {
			opts.WriteTimeout = s.WriteTimeout * time.Second
		}
		rdb = redis.NewClient(opts)
	} else {
		opts := &redis.ClusterOptions{
			Addrs:    endpoints,
			Password: s.Password,
		}
		if s.DialTimeout > 0 {
			opts.DialTimeout = s.DialTimeout * time.Second
		}
		if s.ReadTimeout > 0 {
			opts.ReadTimeout = s.ReadTimeout * time.Second
		}
		if s.WriteTimeout > 0 {
			opts.WriteTimeout = s.WriteTimeout * time.Second
		}
		rdb = redis.NewClusterClient(opts)
	}
	storage := &Redis{
		client: rdb,
	}
	return storage
}
