package store

import (
	"fmt"
	"strings"

	"gitee.com/zfd81/sober/config"
)

const (
	KeyPrefix    = "$"
	KeySeparator = "/"
)

func prefix() string {
	return KeyPrefix + KeySeparator
}

func key(oid string) string {
	return prefix() + oid
}

type object struct {
	id    string
	attrs map[string]string
}

func (o *object) Id() string {
	return o.id
}

func (o *object) Set(attr string, val string) *object {
	o.attrs[attr] = val
	return o
}

func (o *object) Remove(attr string) *object {
	delete(o.attrs, attr)
	return o
}

func (o *object) Values() map[string]string {
	return o.attrs
}

func NewObject(id string) *object {
	return &object{
		id:    id,
		attrs: map[string]string{},
	}
}

type Storage interface {
	Set(oid string, data map[string]interface{}) (int64, error)      //设置对象属性
	Delete(oid string, attrs ...string) (int64, error)               //删除对象属性
	Clear(oid string) (int64, error)                                 //清除对象
	Get(oid string, attrs ...string) (map[string]interface{}, error) //获得对象的指定属性值
	AttrSet(oid string) ([]string, error)                            //获得对象属性集
	Count() (int64, error)                                           //获得对象数量
	BatchSet(data []*object) error                                   //批量设置数据
}

func Open(s *config.Store) (storage Storage, err error) {
	if s == nil {
		return nil, fmt.Errorf("Open storage error")
	}
	switch strings.ToUpper(s.Driver) {
	case "REDIS", "":
		storage = NewRedis(s)
		break
	default:
		break
	}
	return
}
