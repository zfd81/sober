package store

import (
	"fmt"
	"sync"
	"time"

	"gitee.com/zfd81/sober/config"
	"stathat.com/c/consistent"
)

var (
	pool       = map[string]Storage{}
	bufferSize = config.GetConfig().BufferSize
	batchSize  = config.GetConfig().BatchWriteSize
	c          = consistent.New()
)

type Boat struct {
	channel  chan []*object
	capacity int
	data     []*object
	mu       sync.RWMutex
}

func (b *Boat) Channel() chan []*object {
	return b.channel
}

func (b *Boat) Size() int {
	b.mu.RLock()
	defer b.mu.RUnlock()
	return len(b.data)
}

func (b *Boat) Add(obj *object) {
	b.mu.Lock()
	defer b.mu.Unlock()
	b.data = append(b.data, obj)
	if len(b.data) == b.capacity {
		b.channel <- b.data
		b.data = []*object{}
	}
}

func (b *Boat) Clear() int {
	b.mu.Lock()
	defer b.mu.Unlock()
	cnt := len(b.data)
	if cnt > 0 {
		b.channel <- b.data
		b.data = []*object{}
	}
	return cnt
}

func NewBoat() *Boat {
	return &Boat{
		channel:  make(chan []*object, bufferSize),
		capacity: batchSize,
	}
}

type Fleet struct {
	boats map[string]*Boat
	err   error
}

func (f *Fleet) Disabled() bool {
	return f.err != nil
}

func (f *Fleet) GetBoat(index string) *Boat {
	return f.boats[index]
}

func (f *Fleet) SetBoat(index string, boat *Boat) {
	f.boats[index] = boat
}

func (f *Fleet) HasTask() bool {
	for _, b := range f.boats {
		if len(b.channel) > 0 {
			return true
		}
	}
	return false
}

func (f *Fleet) Clear() int {
	cnt := 0
	for _, b := range f.boats {
		cnt = cnt + b.Clear()
	}
	return cnt
}

func (f *Fleet) SetError(err error) {
	f.err = err
}

func (f *Fleet) Wait() error {
	if f.err != nil {
		return f.err
	} else {
		for {
			if f.HasTask() {
				time.Sleep(1 * time.Second)
			} else {
				return nil
			}
		}
	}
}

func NewFleet() *Fleet {
	return &Fleet{
		boats: make(map[string]*Boat),
	}
}

func InitStorages() error {
	stores := config.GetConfig().Stores
	if len(stores) == 0 {
		return fmt.Errorf("Storage resource cannot be empty")
	}
	for _, db := range stores {
		storage, err := Open(&db)
		if err != nil {
			return err
		}
		pool[db.Key()] = storage
		c.Add(db.Key())
	}
	return nil
}

func PoolIndexs() []string {
	return c.Members()
}

func GetStorage(index string) Storage {
	return pool[index]
}

func StorageIndex(key string) (index string) {
	index, _ = c.Get(key)
	return
}

func Set(oid string, data map[string]interface{}) (int64, error) {
	index := StorageIndex(oid)
	return GetStorage(index).Set(oid, data)
}

func Delete(oid string, attrs ...string) (int64, error) {
	index := StorageIndex(oid)
	return GetStorage(index).Delete(oid, attrs...)
}

func Clear(oid string) (int64, error) {
	index := StorageIndex(oid)
	return GetStorage(index).Clear(oid)
}

func Get(oid string, attrs ...string) (map[string]interface{}, error) {
	index := StorageIndex(oid)
	return GetStorage(index).Get(oid, attrs...)
}

func AttrSet(oid string) ([]string, error) {
	index := StorageIndex(oid)
	return GetStorage(index).AttrSet(oid)
}

func Count() (int64, error) {
	var cnt int64
	for _, s := range pool {
		c, err := s.Count()
		if err != nil {
			return c, err
		}
		cnt += c
	}
	return cnt, nil
}

func BatchSet(index string, data []*object) error {
	return GetStorage(index).BatchSet(data)
}
