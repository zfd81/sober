package store

import (
	"context"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"google.golang.org/grpc/resolver"

	"google.golang.org/grpc/resolver/manual"

	"gitee.com/zfd81/sober/config"
	pb "gitee.com/zfd81/sober/store/grpcs/grpcspb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	_ "google.golang.org/grpc/health"
)

type Grpcs struct {
	client pb.GrpcsClient
}

func (db *Grpcs) Set(oid string, data map[string]interface{}) (int64, error) {
	request := &pb.ServRequest{
		Key: oid,
		//Values: (map[string]interface{})data,
	}
	resp, err := db.client.Set(context.Background(), request)
	if err != nil {
		return 0, err
	}
	return resp.Cnt, nil
}

func (db *Grpcs) Delete(oid string, attrs ...string) (int64, error) {
	request := &pb.ServRequest{
		Key:    oid,
		Fields: attrs,
	}
	resp, err := db.client.Delete(context.Background(), request)
	if err != nil {
		return 0, err
	}
	return resp.Cnt, nil
}

func (db *Grpcs) Clear(oid string) (int64, error) {
	request := &pb.ServRequest{
		Key: oid,
	}
	resp, err := db.client.Clear(context.Background(), request)
	if err != nil {
		return 0, err
	}
	return resp.Cnt, nil
}

func (db *Grpcs) Get(oid string, attrs ...string) (map[string]interface{}, error) {
	result := map[string]interface{}{}
	request := &pb.ServRequest{
		Key:    oid,
		Fields: attrs,
	}
	resp, err := db.client.Get(context.Background(), request)
	if err != nil {
		return result, err
	}
	for i, v := range resp.Data {
		result[attrs[i]] = v
	}
	return result, nil
}

func (db *Grpcs) AttrSet(oid string) ([]string, error) {
	request := &pb.ServRequest{
		Key: oid,
	}
	resp, err := db.client.Fields(context.Background(), request)
	if err != nil {
		return nil, err
	}
	return resp.Data, nil
}

func (db *Grpcs) Count() (int64, error) {
	request := &pb.ScanRequest{
		Cursor: 0,
		Match:  fmt.Sprintf("%s*", prefix()),
		Count:  -1,
	}
	resp, err := db.client.Scan(context.Background(), request)
	if err != nil {
		return -1, err
	}
	var cnt int64
	for {
		_, err = resp.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return -1, err
		}
		cnt++
	}
	return cnt, nil
}

func (db *Grpcs) BatchSet(data []*object) error {
	stream, err := db.client.BatchSet(context.Background())
	if err != nil {
		return err
	}
	for _, obj := range data {
		err = stream.Send(&pb.DataPacket{
			Key:    obj.id,
			Values: obj.Values(),
		})
		if err != nil {
			stream.CloseAndRecv()
			return err
		}
	}
	_, err = stream.CloseAndRecv()
	return err
}

func NewGrpcs(s *config.Store) *Grpcs {
	var addresses []resolver.Address
	endpoints := strings.Split(s.Address, ",")
	for _, endpoint := range endpoints {
		addresses = append(addresses, resolver.Address{Addr: endpoint})
	}
	r := manual.NewBuilderWithScheme("whatever")
	r.InitialState(resolver.State{Addresses: addresses})
	conn, err := grpc.Dial(
		r.Scheme()+":///grpcs.server",
		grpc.WithInsecure(),
		grpc.WithResolvers(r),
		grpc.WithDefaultServiceConfig(fmt.Sprintf(`{"LoadBalancingPolicy": "%s", "RetryPolicy": {"MaxAttempts":2, "InitialBackoff": "0.1s", "MaxBackoff": "1s", "BackoffMultiplier": 2.0, "RetryableStatusCodes": ["UNAVAILABLE"]}}`, roundrobin.Name)),
		grpc.WithBlock(),
		grpc.WithTimeout(3*time.Second))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	storage := &Grpcs{
		client: pb.NewGrpcsClient(conn),
	}
	return storage
}
