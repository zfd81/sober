package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	"gitee.com/zfd81/sober/cluster"

	"gitee.com/zfd81/sober/log"

	"gitee.com/zfd81/sober/expression/functions"

	"gitee.com/zfd81/sober/store"

	"golang.org/x/sync/errgroup"

	"gitee.com/zfd81/sober/config"
	pb "gitee.com/zfd81/sober/proto/soberpb"
	"gitee.com/zfd81/sober/server"
	"github.com/fatih/color"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

var (
	conf    = config.GetConfig()
	g       errgroup.Group
	rootCmd = &cobra.Command{
		Use:        "plume",
		Short:      "plume server",
		SuggestFor: []string{"plume"},
		Run:        startCommandFunc,
	}
	port  uint32
	cport uint32
)

func init() {
	//设置命令行参数
	rootCmd.Flags().Uint32Var(&port, "port", conf.Port, "Port to run the server")
	rootCmd.Flags().Uint32Var(&cport, "p", conf.Cport, "Sober communication port")

	//设置日志打印格式
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05", //时间格式
	})

	//设置日志级别
	level := logrus.InfoLevel
	switch strings.ToUpper(conf.Log.Level) {
	case "ERROR":
		level = logrus.ErrorLevel
	case "WARN":
		level = logrus.WarnLevel
	case "DEBUG":
		level = logrus.DebugLevel
	case "TRACE":
		level = logrus.TraceLevel
	}
	logrus.SetLevel(level)

}

func startCommandFunc(cmd *cobra.Command, args []string) {
	//根据命令行参数修改配置信息
	conf.Port = port
	conf.Cport = cport

	hook, err := log.NewFileHook(fmt.Sprintf("./logs/sober_%d", conf.Port))
	logrus.AddHook(hook)

	//打印配置信息
	logrus.Info("Sober version: ", conf.Version)
	//logrus.Info("Rock log Level: ", config.Log.Level)

	logrus.Info("Sober Buffer Size: ", conf.BufferSize)
	logrus.Info("Sober Batch Write Size: ", conf.BatchWriteSize)

	//打印存储池信息
	logrus.Info("Sober storage pool: ")
	stores := conf.Stores
	for i, s := range stores {
		logrus.Infof("%d) driver=%s address=%s db=%s", i+1, functions.Decode(s.Driver, "", "redis"), s.Address, s.Database)
	}

	//创建RPC服务
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Port))
	if err != nil {
		logrus.Fatalf("failed to listen: %v", err)
	}
	soberServ := grpc.NewServer()
	pb.RegisterSoberServer(soberServ, &server.Sober{})
	pb.RegisterSoberSystemServer(soberServ, &server.SoberSystem{})
	pb.RegisterClusterServer(soberServ, &server.Cluster{})
	//启动Server
	g.Go(func() error {
		err := soberServ.Serve(lis)
		if err != nil {
			logrus.Errorf("failed to api serve: %v", err)
		}
		return err
	})

	//初始化Storage
	store.InitStorages()

	//集群注册
	cluster.Register()

	//打印启动成功日志
	time.Sleep(time.Duration(1) * time.Second)
	logrus.Infof("Gossip listening on: %d", conf.Cport)
	logrus.Infof("Sober server started successfully, listening on: %d", conf.Port)

	if err := g.Wait(); err != nil {
		logrus.Fatal(err)
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			logrus.Error(err)
		}
	}()
	color.Green(conf.Banner)
	Execute()
}
