# sober

## Language Definition

### Supported data types

* **strings** - single and double quotes (e.g. `"hello"`, `'hello'`)
* **numbers** - e.g. `103`, `2.5`, `.5`
* **arrays** - e.g. `[1, 2, 3]`
* **maps** - e.g. `{foo: "bar"}`
* **booleans** - `true` and `false`
* **nil** - `nil`

### Supported Operators

#### Arithmetic Operators

* `+` (addition)
* `-` (subtraction)
* `*` (multiplication)
* `/` (division)
* `%` (modulus)
* `**` (pow)

Example:

```js
life + universe + everything
``` 

#### Comparison Operators

* `==` (equal)
* `!=` (not equal)
* `<` (less than)
* `>` (greater than)
* `<=` (less than or equal to)
* `>=` (greater than or equal to)

#### Logical Operators

* `not` or `!`
* `and` or `&&`
* `or` or `||`

Example:

```
life < universe || life < everything
```

#### String Operators

* `+` (concatenation)
* `matches` (regex match)
* `contains` (string contains)
* `startsWith` (has prefix)
* `endsWith` (has suffix)

To test if a string does *not* match a regex, use the logical `not` operator in combination with the `matches` operator:

```js
not ("foo" matches "^b.+")
```

You must use parenthesis because the unary operator `not` has precedence over the binary operator `matches`.

Example:

```js
'Arthur' + ' ' + 'Dent'
```

Result will be set to `Arthur Dent`.

#### Membership Operators

* `in` (contain)
* `not in` (does not contain)

Example:

```js
user.Group in ["human_resources", "marketing"]
```

```js
"foo" in {foo: 1, bar: 2}
```

#### Numeric Operators

* `..` (range)

Example:

```js
user.Age in 18..45
```

The range is inclusive:

```js
1..3 == [1, 2, 3]
```

#### Ternary Operators

* `foo ? 'yes' : 'no'`

Example:

```js
user.Age > 30 ? "mature" : "immature"
```

### Builtin functions

#### len(obj)
length of array, map or string

```js
len([1,2,3,4,5]) // return 5
len("hello world") // return 11
len({foo: "bar"}) // return 1
```

#### if(condition, if_true, if_false)
if/else function

```js
if(1>2, 'ok', 'no') // return 'no'
if(2>1, 'ok', 'no') // return 'ok'
```

#### decode(condition, if1, then1, if2, then2,... , default)
logic processing function

```js
decode(23, 1, "ok1", 23, "ok2") // return "ok2"
decode(23, 1, "ok1", 2, "ok2", "default") // return "default"
decode(23, 1, "ok1", 2, "ok2") // return 23
decode(23, 1, "ok1", 2) // return 2
decode(23, 1, "ok1") // return 23
decode(23, 1) // return 23
decode(23) // return 23
```

#### eq(val1, val2)
equality function

```js
eq(2, 3) // return false
eq("str1", "str1") // return true
eq(false, false) // return true
eq("23", 23) // return false
```

#### left(str, length)
returns the leftmost length characters of str

```js
left("abc", -1) // return ""
left("abc", 0) // return ""
left("abc", 2) // return "ab"
left("abc", 5) // return "abc"
```

#### right(str, length)
returns the rightmost length characters of str

```js
right("abc", -1) // return ""
right("abc", 0) // return ""
right("abc", 2) // return "bc"
right("abc", 5) // return "abc"
```

#### substr(str, position, length)
returns a new string that is a substring of str.
- position starts from 1
```js
substr("123456", 1, 2) // return "12"
substr("123456", 1, 8) // return "123456"
substr("123456", 1, 0) // return ""
substr("123456", 0, 2) // return ""
right("abc", 5) // return "abc"
```

#### split(str, sep)
split the str into a string array by the separator sep.

```js
split("ab,cd,ef", ",") // return ["ab" "cd" "ef"]
split("ab,cd,ef", "") // return ["a" "b" "," "c" "d" "," "e" "f"]
split(",ab,cd,ef", ",") // return ["" "ab" "cd" "ef"]
split("ab,cd,ef,", ",") // return ["ab" "cd" "ef" ""]
```

#### splitn(str, sep, n)
the string str is divided into string arrays by the separator sep, and the parameter n determines the number of returned arrays

- n > 0: at most n substrings; the last substring will be the unsplit remainder.
- n == 0: the result is nil (zero substrings)
- n < 0: all substrings
```js
splitn("ab,cd,ef", ",", 5) // return ["ab" "cd" "ef"]
splitn("ab,cd,ef", ",", 2) // return ["ab" "cd,ef"]
splitn("ab,cd,ef", ",", 0) // return []
splitn("ab,cd,ef", ",", -1) // return ["ab" "cd" "ef"]
splitn("ab,cd,ef", "", 5) // return ["a" "b" "," "c" "d,ef"]
splitn(",ab,cd,ef", ",", 5) // return ["" "ab" "cd" "ef"]
splitn("ab,cd,ef,", ",", 2) // return ["ab" "cd,ef,"]
```

#### obj(str)
parse JSON-encoded data and return a map object.  

Variable `str` is `{"name":"zfd","age":"23","tel":"180XXXXX081"}`.
```js
obj(str).name // return "zfd"
obj(str).age // return 23
obj(str)["tel"] // return "180XXXXX081"
len(obj(str)) // return 3
```

#### num(val)
convert to numbers.

```js
num("123") // return 123
num("123.55") // return 123.55
num("true") // return 1
num("false") // return 0
num(123) // return 123
num(123.55) // return 123.55
num(true) // return 1
num(false) // return 0
```

#### datetime
time type function.

##### time([datetime][,format])
return datetime object.

- datetime: specified time; can be empty default is current time
- format: time format; can be empty default is "yyyyMMddHHmmss"
```js
time() // return "20220212125015"
time("20220212125015") // return "20220212125015"
time("2022-02-12 12:50:15") // error: the time format does not match because the default format is "yyyyMMddHHmmss"
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss") // return "2022-02-12 12:50:15"
```

##### Year()
returns the year in which occurs.

```js
time().Year() // return 2022
time("20220212125015").Year() // return 2022
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").Year() // return 2022
```

##### Month()
returns the month of the year specified time.

```js
time().Month() // return 2
time("20220212125015").Month() // return 2
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").Month() // return 2
```

##### Day()
returns the day of the month specified time.

```js
time().Day() // return 12
time("20220212125015").Day() // return 12
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").Day() // return 12
```

##### YearDay()
returns the day of the year specified time.

```js
time().YearDay() // return 43
time("20220212125015").YearDay() // return 43
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").YearDay() // return 43
```

##### WeekDay()
returns the day of the week specified time.

```js
time().WeekDay() // return 6
time("20220212125015").WeekDay() // return 6
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").WeekDay() // return 6
```

##### AddHours(hours)
Add hours to the specified time.

```js
time("20220212125015").AddHours(7) // return "20220212195015"
time("20220212125015").AddHours(-25).Day() // return 11
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").AddHours(7) // return "2022-02-12 19:50:15"
```

##### AddDays(days)
Add days to the specified time.

```js
time("20220212125015").AddDays(7) // return "20220219125015"
time("20220212125015").AddDays(7).Day() // return 19
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").AddDays(7) // return "2022-02-19 12:50:15"
```

##### Operator(+) Override
Add days to the specified time.

```js
time("20220212125015") + 7 // return "20220219125015"
(time("20220212125015") + 7).Day() // return 19
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss") + 7 // return "2022-02-19 12:50:15"
```

##### AddMonths(months)
Add months to the specified time.

```js
time("20220212125015").AddMonths(7) // return "20220912125015"
time("20220212125015").AddMonths(11).Month() // return 1
time("20220212125015").AddMonths(11).Year() // return 2023
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").AddMonths(7) // return "2022-09-12 12:50:15"
```

##### DiffHour(datetime)
Calculate the difference in hours.

```js
time("20220212125015").DiffHour("20220212155015") // return -3
time("2022-02-12 12:50:15", "yyyy-MM-dd HH:mm:ss").DiffHour("2022-02-12 15:50:15") // return -3
```

##### DiffDay(datetime)
Calculate the difference in days.

```js
time("20220212125015").DiffDay("20220212155015") // return 0
time("20220212125015").DiffDay("20220218155015") // return -6
time("2022-03-12 12:50:15", "yyyy-MM-dd HH:mm:ss").DiffDay("2022-02-12 15:50:15") // return 28
```

##### Operator(-) Override
Calculate the difference in days.

```js
time("20220212125015") - "20220212155015" // return 0
time("20220212125015") - "20220218155015" // return -6
time("2022-03-12 12:50:15", "yyyy-MM-dd HH:mm:ss") - "2022-02-12 15:50:15" // return 28
```

### Slices

* `array[:]` (slice)

Slices can work with arrays or strings.

Example:

Variable `array` is `[1,2,3,4,5]`.

```js
array[1:5] // return [2,3,4,5] 
array[3:] // return [4,5]
array[:4] // return [1,2,3,4]
array[:] // return array
```

