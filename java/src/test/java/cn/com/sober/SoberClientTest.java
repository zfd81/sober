package cn.com.sober;

import junit.framework.TestCase;

import java.util.HashMap;
import java.util.Map;
import java.io.*;

public class SoberClientTest extends TestCase {
    private Options options = new Options(new String[]{"127.0.0.1:2022"});
    private SoberClient cli = new SoberClient(options);

    public void testSet() {
        String oid = "dada";
        Map<String, Object> data = new HashMap<>();
        data.put("name", "zfd2");
        data.put("age", 232);
        data.put("sex", true);
        try {
            int cnt = cli.Set(oid, data);
            System.out.println(cnt);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }


    public void testDelete() {
        String oid = "dada";
        String content = "age22,age";
        try {
            int cnt = cli.Delete(oid, content);
            System.out.println(cnt);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }

    public void testClear() {
        String oid = "dada";
        try {
            int cnt = cli.Clear(oid);
            System.out.println(cnt);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }

    public void testGet() {
        String oid = "dada";
        try {
            Map<String, Object> m = cli.Get(oid);
            System.out.println(m);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }

        try {
            Map<String, Object> m = cli.Get(oid, "name,age*10");
            System.out.println(m);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }

    public void testIs() {
        String oid = "dada";
        try {
            boolean bool = cli.Is(oid, "age>100");
            System.out.println(bool);
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }

    public void testAttributeSet() {
        String oid = "dada";
        try {
            String[] attrs = cli.AttributeSet(oid);
            for (int i = 0; i < attrs.length; i++) {
                System.out.println(attrs[i]);
            }
        } catch (SoberGrpcException e) {
            System.out.println(e);
        }
    }

    public void testBatchSet() {
        try {
            File file = new File("data.csv");
            System.out.println(file.getAbsolutePath());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            String[] cols = line.split(",");
            int cnt = cli.BatchSet(cols,br);
            System.out.println(cnt);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}