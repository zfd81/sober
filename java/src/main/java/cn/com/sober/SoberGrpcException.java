package cn.com.sober;

public class SoberGrpcException extends Exception {

    public SoberGrpcException() {
    }

    public SoberGrpcException(String message) {
        super(message);
    }

    public SoberGrpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoberGrpcException(Throwable cause) {
        super(cause);
    }

    public SoberGrpcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
