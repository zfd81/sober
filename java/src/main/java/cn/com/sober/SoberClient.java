package cn.com.sober;

import io.grpc.ManagedChannel;
import cn.com.sober.protobuf.*;
import cn.com.sober.protobuf.SoberService.*;
import io.grpc.ManagedChannelBuilder;

import java.io.BufferedReader;
import java.io.IOException;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import java.util.*;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import io.grpc.stub.StreamObserver;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class SoberClient {

    private static final Logger logger = LoggerFactory.getLogger(SoberClient.class);
    private ManagedChannel managedChannel;
    private SoberGrpc.SoberBlockingStub blockingStub;
    private SoberGrpc.SoberStub stub;

    public SoberClient(Options options) {
        String endpoint = options.getEndpoints()[0];
        int index = endpoint.indexOf(":");
        managedChannel = ManagedChannelBuilder.forAddress(endpoint.substring(0, index), Integer.parseInt(endpoint.substring(index + 1))).usePlaintext().build();
        blockingStub = SoberGrpc.newBlockingStub(managedChannel);
        stub = SoberGrpc.newStub(managedChannel);
    }

    public int Set(String oid, Map<String, Object> attributes) throws SoberGrpcException {
        checkOid(oid);
        if (attributes == null || attributes.isEmpty()) {
            throw new SoberGrpcException("Attributes cannot be empty");
        }
        Map<String, String> data = new HashMap<>();
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            data.put(entry.getKey(), String.valueOf(entry.getValue()));
        }
        Request request = Request.newBuilder().setOid(oid).putAllData(data).build();
        try {
            Response resp = blockingStub.set(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            return (int) resp.getCnt();
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public int Delete(String oid, String content) throws SoberGrpcException {
        checkOid(oid);
        if (content == null || content.trim().equals("")) {
            throw new SoberGrpcException("Deleted content cannot be empty");
        }
        Request request = Request.newBuilder().setOid(oid).setContent(content).build();
        try {
            Response resp = blockingStub.delete(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            return (int) resp.getCnt();
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public int Clear(String oid) throws SoberGrpcException {
        checkOid(oid);
        Request request = Request.newBuilder().setOid(oid).build();
        try {
            Response resp = blockingStub.clear(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            return (int) resp.getCnt();
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public Map<String, Object> Get(String oid, String content) throws SoberGrpcException {
        checkOid(oid);
        Request request = Request.newBuilder().setOid(oid).setContent(content).build();
        try {
            QueryResponse resp = blockingStub.get(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            Map<String, Object> result = new HashMap<>();
            Map<String, String> data = resp.getDataMap();
            if (!data.isEmpty()) {
                for (Map.Entry<String, String> entry : resp.getDataMap().entrySet()) {
                    result.put(entry.getKey(), DataConversion(entry.getValue()));
                }
            }
            return result;
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public Map<String, Object> Get(String oid) throws SoberGrpcException {
        return Get(oid, "");
    }

    public boolean Is(String oid, String conditions) throws SoberGrpcException {
        checkOid(oid);
        if (conditions == null || conditions.trim().equals("")) {
            throw new SoberGrpcException("Judgment conditions cannot be empty");
        }
        Request request = Request.newBuilder().setOid(oid).setCondition(conditions).build();
        try {
            JudgmentResponse resp = blockingStub.is(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            return resp.getSignal();
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public String[] AttributeSet(String oid) throws SoberGrpcException {
        checkOid(oid);
        Request request = Request.newBuilder().setOid(oid).build();
        try {
            Response resp = blockingStub.attributeSet(request);
            logger.debug(String.format("Execution time: %s", stampToDate(resp.getTimestamp())));
            return resp.getAttributesList().toArray(new String[0]);
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        }
    }

    public int BatchSet(String[] cols, BufferedReader br) throws SoberGrpcException {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final Response.Builder respb = Response.newBuilder();
        StreamObserver<Response> responseObserver = new StreamObserver<Response>() {
            @Override
            public void onNext(Response value) {
                respb.setCnt(value.getCnt());
            }

            @Override
            public void onError(Throwable t) {
                logger.debug(String.format("Batch set error: %s", t.toString()));
                respb.setCnt(-1);
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                countDownLatch.countDown();
            }
        };

        StreamObserver<StreamRequest> requestObserver = stub.batchSet(responseObserver);
        try {
            if (cols == null || cols.length == 0) {
                throw new SoberGrpcException("Attribute name cannot be empty");
            }
            final StringJoiner joiner = new StringJoiner(",");
            for (String col : cols) {
                joiner.add(col);
            }
            StreamRequest line = StreamRequest.newBuilder().setData(joiner.toString()).build();
            requestObserver.onNext(line);

            String tempString = null;
            while ((tempString = br.readLine()) != null) {
                line = StreamRequest.newBuilder().setData(tempString).build();
                requestObserver.onNext(line);
            }
            requestObserver.onCompleted();
            br.close();

            //如果在规定时间内没有请求完，则让程序抛出异常
            if (!countDownLatch.await(120, TimeUnit.MINUTES)) {
                throw new SoberGrpcException("Batch setting timeout");
            }
            logger.debug(String.format("Execution time: %s", stampToDate(respb.getTimestamp())));
            return (int) respb.getCnt();
        } catch (Exception e) {
            throw new SoberGrpcException(e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    private void checkOid(String oid) throws SoberGrpcException {
        if (oid == null || oid.trim().equals("")) {
            throw new SoberGrpcException("Object ID cannot be empty");
        }
    }

    private String stampToDate(long s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(s * 1000);
        return simpleDateFormat.format(date);
    }

    private Object DataConversion(String str) {
        if (Pattern.matches("^0||[1-9]\\d+$", str)) {
            return Integer.parseInt(str);
        } else if (Pattern.matches("^\\d+[.]\\d+$", str)) {
            return Float.parseFloat(str);
        } else if (str.trim().equals("true") || str.trim().equals("false")) {
            return Boolean.parseBoolean(str);
        } else {
            return str;
        }
    }
}
