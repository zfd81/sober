package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var filename = "data1.csv"
	f, err := os.Create(filename) //创建文件
	if err != nil {
		fmt.Println(err.Error())
	}
	w := bufio.NewWriter(f) //创建新的 Writer 对象
	_, err = w.WriteString("id,name,age,sex,col1,col2,col3,col4,col5,col6\n")
	for i := 1000000; i < 200; i++ {
		_, err = w.WriteString(fmt.Sprintf("id%d,", i))
		_, err = w.WriteString(fmt.Sprintf("name%d,", i))
		_, err = w.WriteString(fmt.Sprintf("%d,", i))
		_, err = w.WriteString(fmt.Sprintf("true,"))
		_, err = w.WriteString(fmt.Sprintf("col1_%d,", i))
		_, err = w.WriteString(fmt.Sprintf("col2_%d,", i))
		_, err = w.WriteString(fmt.Sprintf("col3_%d,", i))
		_, err = w.WriteString(fmt.Sprintf("col4_%d,", i))
		_, err = w.WriteString(fmt.Sprintf("col5_%d,", i))
		_, err = w.WriteString(fmt.Sprintf("col6_%d", i))
		_, err = w.WriteString("\n")
	}
	w.Flush()
	f.Close()
}
