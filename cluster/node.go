package cluster

import (
	"fmt"
	"runtime"
	"time"

	"github.com/spf13/cast"
	"github.com/zfd81/rooster/util"

	"github.com/hashicorp/memberlist"
)

type Node struct {
	*memberlist.Node
	ServPort    uint32
	StartUpTime int64 `json:"start-up-time"`
}

func (n *Node) FullAddress() string {
	return fmt.Sprintf("%s:%d", n.Addr.String(), n.ServPort)
}

func (n *Node) MemStats() (ms runtime.MemStats) {
	runtime.ReadMemStats(&ms)
	return
}

func NewNode() *Node {
	return &Node{
		StartUpTime: time.Now().Unix(),
	}
}

func CreateNode(node *memberlist.Node) *Node {
	return &Node{
		Node:        node,
		StartUpTime: cast.ToInt64(util.Left(node.Name, 10)),
		ServPort:    cast.ToUint32(util.Right(node.Name, 4)),
	}
}
