package cluster

import (
	"fmt"

	"github.com/zfd81/rooster/util"

	"github.com/sirupsen/logrus"

	"github.com/hashicorp/memberlist"
)

type eventDelegate struct{}

// NotifyJoin 当检测到节点已加入时，将调用NotifyJoin。
// 不能修改节点参数。
func (ed *eventDelegate) NotifyJoin(node *memberlist.Node) {
	n := CreateNode(node)
	mtx.Lock()
	members[n.FullAddress()] = n
	mtx.Unlock()
	logrus.Info("A node has joined: " + node.String())
	if IsLeader() {
		broadcasts.QueueBroadcast(&broadcast{
			msg:    Packet(ElectINSTR, leader.Id, leader.TimeStamp),
			notify: nil,
		})
	}
}

// NotifyLeave 当检测到节点已离开时，将调用NotifyLeave。
// 不能修改节点参数。
func (ed *eventDelegate) NotifyLeave(node *memberlist.Node) {
	mtx.Lock()
	delete(members, fmt.Sprintf("%s:%s", node.Addr.String(), util.Right(node.Name, 4)))
	mtx.Unlock()
	logrus.Warn("A node has left: " + node.String())
	if len(members) == 1 {
		leader.BeElected()
	}
}

// NotifyUpdate 当检测到节点已更新时(通常涉及元数据)，将调用NotifyUpdate。
// 不能修改节点参数。
func (ed *eventDelegate) NotifyUpdate(node *memberlist.Node) {
	n := CreateNode(node)
	mtx.Lock()
	members[n.FullAddress()] = n
	mtx.Unlock()
	logrus.Info("A node was updated: " + node.String())
}
