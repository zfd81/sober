package cluster

import (
	"gitee.com/zfd81/sober/util"

	"github.com/sirupsen/logrus"
)

const (
	ElectINSTR byte = iota
	AddINSTR
	RemoveINSTR
)

type delegate struct{}

// NodeMeta 用于在广播活动消息时检索有关当前节点的元数据。
// 它的长度限制为给定的字节大小。此元数据在节点结构中可用。
func (d *delegate) NodeMeta(limit int) []byte {
	return []byte{}
}

// NotifyMsg 收到用户数据消息时调用NotifyMsg。
// 应注意此方法不会阻塞，因为这样做会阻塞整个UDP数据包接收循环。
// 此外，可以在调用返回后修改字节片，因此如果需要，应该复制它
func (d *delegate) NotifyMsg(bytes []byte) {
	length := len(bytes)
	if length == 0 {
		return
	}
	switch bytes[0] {
	case ElectINSTR:
		id := string(bytes[9 : length-8])
		if leader.Id != id {
			mtx.Lock()
			leader.Id = id
			leader.TimeStamp = int64(util.BytesToInt(bytes[length-8:]))
			mtx.Unlock()
			logrus.Infof("%s endpoint elected leader", leader.Id)
		}

	}
}

// GetBroadcasts 当可以广播用户数据消息时，调用GetBroadcasts。
// 它可以返回要发送的缓冲区列表。每个缓冲区应承担一定的开销，并对允许的总字节大小进行限制。
// 要发送的结果数据的总字节大小不得超过限制。
// 应注意此方法不会阻塞，因为这样做会阻塞整个UDP数据包接收循环。
func (d *delegate) GetBroadcasts(overhead, limit int) [][]byte {
	return broadcasts.GetBroadcasts(overhead, limit)
}

// LocalState 用于TCP推/拉。
// 除了成员信息之外，还会将其发送到远程端。任何数据都可以发送到这里。
// 请参见MergeRemoteState。“join”布尔值表示这是用于连接，而不是推/拉。
// 每隔PushPullInterval周期，本地memberlist回调LocalState方法，把本地全部数据发送到其他节点；
// 其他节点memberlist回调MergeRemoteState，接收数据进行同步。
func (d *delegate) LocalState(join bool) []byte {
	return nil
}

// MergeRemoteState 在TCP推/拉之后调用MergeRemoteState。
// 这是从远程端接收到的状态，是远程端LocalState调用的结果。“join”布尔值表示这是用于连接，而不是推/拉。
func (d *delegate) MergeRemoteState(buf []byte, join bool) {
	if len(buf) == 0 {
		return
	}
	if !join {
		return
	}
}
