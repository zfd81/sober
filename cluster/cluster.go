package cluster

import (
	"fmt"
	"sync"
	"time"

	"gitee.com/zfd81/sober/util"

	"github.com/sirupsen/logrus"

	"github.com/spf13/cast"

	"gitee.com/zfd81/sober/config"

	"github.com/hashicorp/memberlist"
	"github.com/zfd81/rooster/types/container"
	rutil "github.com/zfd81/rooster/util"
)

var (
	mtx        sync.RWMutex
	conf       = config.GetConfig()
	broadcasts *memberlist.TransmitLimitedQueue
	node       *Node
	members    = map[string]*Node{}
	leader     = &Leader{}
)

type Leader struct {
	Id        string
	TimeStamp int64
}

func (l *Leader) BeElected() *Leader {
	l.Id = CurrentNode().FullAddress()
	l.TimeStamp = time.Now().Unix()
	return l
}
func (l *Leader) LostElected() {
	l.Id = ""
	l.TimeStamp = time.Now().Unix()
}

type broadcast struct {
	msg    []byte
	notify chan<- struct{}
}

func (b *broadcast) Invalidates(other memberlist.Broadcast) bool {
	return false
}

func (b *broadcast) Message() []byte {
	return b.msg
}

func (b *broadcast) Finished() {
	if b.notify != nil {
		close(b.notify)
	}
}

func GetLeaderNode() *Node {
	return members[leader.Id]
}

func IsLeader() bool {
	if leader == nil || leader.Id == "" {
		return false
	}
	if lnode := GetLeaderNode(); lnode != nil {
		return CurrentNode().FullAddress() == lnode.FullAddress()
	}
	return false
}

func CurrentNode() *Node {
	return node
}

func Register() error {
	node = NewNode()
	node.ServPort = conf.Port
	c := memberlist.DefaultLocalConfig()
	c.Events = &eventDelegate{}
	c.Delegate = &delegate{}
	c.BindPort = int(conf.Cport)
	c.Name = fmt.Sprintf("%d%d", node.StartUpTime, conf.Port)
	cluster, err := memberlist.Create(c)
	if err != nil {
		panic("Failed to create memberlist: " + err.Error())
	}
	node.Node = cluster.LocalNode()
	set := make(container.HashSet)
	set.Add(fmt.Sprintf("%s:%d", node.Addr.String(), node.Port)) //添加自身真实地址
	for _, m := range conf.Members {
		set.Add(m) //添加成员列表
	}
	set.Remove(fmt.Sprintf("%s:%d", "127.0.0.1", node.Port)) //移除自身本机地址
	set.Remove(fmt.Sprintf("%s:%d", "localhost", node.Port)) //移除自身本机地址

	//构建成员列表
	addresses := make([]string, 0)
	set.Iterator(func(index int, value interface{}) {
		addresses = append(addresses, cast.ToString(value))
	})

	//Join an existing cluster by specifying known members.
	_, err = cluster.Join(addresses)
	if err != nil {
		panic("Failed to join cluster: " + err.Error())
	}

	broadcasts = &memberlist.TransmitLimitedQueue{
		NumNodes: func() int {
			return cluster.NumMembers()
		},
		RetransmitMult: 3, // 最大重新传输的次数
	}

	logrus.Info("Cluster members list: ")
	mtx.Lock()
	for _, member := range cluster.Members() {
		n := &Node{
			Node:        member,
			StartUpTime: cast.ToInt64(rutil.Left(member.Name, 10)),
			ServPort:    cast.ToUint32(rutil.Right(member.Name, 4)),
		}
		members[n.FullAddress()] = n
		logrus.Infof(" - Member: %s %s:%s\n", rutil.Left(member.Name, 10), member.Addr, rutil.Right(member.Name, 4))
	}
	mtx.Unlock()
	if len(members) == 1 {
		leader.BeElected()
	}
	return nil
}

func Members() map[string]*Node {
	return members
}

func Packet(instr byte, content string, timestamp int64) []byte {
	c := []byte(content)
	pack := []byte{instr}                           //报文第1位是指令编码
	pack = append(pack, util.IntToBytes(len(c))...) //报文2~9位是内容长度
	pack = append(pack, c...)
	pack = append(pack, util.IntToBytes(int(timestamp))...) //报文后8位是时间戳
	return pack
}
