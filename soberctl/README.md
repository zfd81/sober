# soberctl

`soberctl` is a command line client for sober.

## Data manipulation commands

### set \<oid\> \<attribute\> \<value\>

Sets object attributes.

#### Examples


```bash
./soberctl set id001 name sorber
# 1
# Elapsed time: 3.905518ms 
./soberctl set id001 age 23
# 1
# Elapsed time: 2.080099ms
```

### del \<oid\> \<attributes\>

Deletes object attributes.

#### Examples
```bash
./soberctl del id001 name,age 
# 2
# Elapsed time: 10.482935ms
```

### clear \<oid\>

Clears object.

#### Examples
```bash
./soberctl clear id001 
# 2
# Elapsed time: 7.140503ms
```

### get \<oid\> [expressions...]

Gets object information.

#### Examples
```bash
./soberctl get id001
# 1) name = sorber
# 2) age = 23
# 3) tel = 180XXXX5081
# Elapsed time: 5.365408ms
./soberctl get id001 name,age
# 1) age = 23
# 2) name = sorber
# Elapsed time: 4.031658ms
./soberctl get id001 age>50,age+10
# 1) age>50 = false
# 2) age+10 = 33
# Elapsed time: 6.265328ms
```

### is \<oid\> \<condition\>

Judgment object.

#### Examples

```bash
./soberctl is id001 name=='sorber'
# true
# Elapsed time: 3.048152ms
./soberctl is id001 name=='sorber' and age>50
# false
# Elapsed time: 5.65283ms
```

### attr \<oid\>

Gets object attribute set.

#### Examples
```bash
./soberctl attr id001
# [name age tel]
# Total 3 attributes
# Elapsed time: 3.877521ms 
```

### cnt

Number of statistical objects.

#### Examples

```bash
./soberctl cnt
# Total objects 5,000,000
# Elapsed time: 6.066240655s
```

### load \<file\>

Bulk load data.

#### File format description

- attribute line -- the first line of the file is the attribute name

- primary key column -- the first column of the file is the primary key column

- separator -- the separator between columns is a comma ","

#### Examples

```bash
./soberctl load ../../data.csv
# 100 / 100 [-------------------------------------------------------------------------------------] 100.00%
# Record Count: 10000000
# Elapsed time: 5m34.596914252s
```

### test

Test the status of the sorber service.

#### Examples

```bash
./soberctl test
# 1) Test set object attributes
# OK
# 2) Test get object attributes
# OK
# 3) Test delete object attributes
# OK
# 4) Test clear object
# OK
# Elapsed time: 34.68062ms
```