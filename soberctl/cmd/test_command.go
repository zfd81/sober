package cmd

import (
	"context"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"

	"github.com/spf13/cobra"
)

func NewTestCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "test",
		Short: "Tests the state of sober",
		Run:   TestCommandFunc,
	}
	return cmd
}

func TestCommandFunc(cmd *cobra.Command, args []string) {
	oid := "soberId"
	request := &pb.Request{
		Oid: oid,
		Data: map[string]string{
			"name": "sober",
			"desc": "object database",
		},
	}
	startTime := time.Now()
	Print("1) Test set object attributes")
	_, err := GetSoberClient().Set(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
	} else {
		Print("OK")
	}

	Print("2) Test get object attributes")
	_, err = GetSoberClient().Get(context.Background(), &pb.Request{Oid: oid})
	if err != nil {
		Errorf(err.Error())
	} else {
		Print("OK")
	}

	Print("3) Test delete object attributes")
	request.Content = "desc"
	_, err = GetSoberClient().Delete(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
	} else {
		Print("OK")
	}

	Print("4) Test clear object")
	_, err = GetSoberClient().Clear(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
	} else {
		Print("OK")
	}

	Print("Elapsed time: %v", time.Since(startTime))
}
