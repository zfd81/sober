package cmd

import (
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/resolver/manual"

	"google.golang.org/grpc/resolver"

	pb "gitee.com/zfd81/sober/proto/soberpb"

	"google.golang.org/grpc"
)

type GlobalFlags struct {
	Endpoints []string
	User      string
	Password  string
}

func GetConnection() *grpc.ClientConn {
	var addresses []resolver.Address
	for _, endpoint := range globalFlags.Endpoints {
		addresses = append(addresses, resolver.Address{Addr: endpoint})
	}

	r := manual.NewBuilderWithScheme("whatever")
	r.InitialState(resolver.State{Addresses: addresses})
	conn, err := grpc.Dial(
		r.Scheme()+":///grpcs.server",
		grpc.WithInsecure(),
		grpc.WithResolvers(r),
		grpc.WithDefaultServiceConfig(fmt.Sprintf(`{"LoadBalancingPolicy": "%s", "RetryPolicy": {"MaxAttempts":2, "InitialBackoff": "0.1s", "MaxBackoff": "1s", "BackoffMultiplier": 2.0, "RetryableStatusCodes": ["UNAVAILABLE"]}}`, roundrobin.Name)),
		grpc.WithBlock(),
		grpc.WithTimeout(3*time.Second))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return conn
}

func GetSoberClient() pb.SoberClient {
	return pb.NewSoberClient(GetConnection())
}

func GetSystemClient() pb.SoberSystemClient {
	return pb.NewSoberSystemClient(GetConnection())
}

func GetClusterClient() pb.ClusterClient {
	return pb.NewClusterClient(GetConnection())
}
