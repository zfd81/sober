package cmd

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	pb3 "github.com/cheggaaa/pb/v3"
	"github.com/spf13/cobra"
)

const (
	OneK = 1024
	OneM = 1024 * OneK
	OneG = 1024 * OneM
)

func NewLoadCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "load <file>",
		Short: "Loads data file",
		Run:   LoadCommandFunc,
	}
	return cmd
}

func LoadCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		ExitWithError(ExitBadArgs, fmt.Errorf("load command requires data file path as its argument"))
	}

	path := args[0]
	info, err := os.Stat(path)
	if err != nil || info.IsDir() {
		Errorf("open %s: No such file", path)
		return
	}

	file, err := os.Open(path)
	if err != nil {
		Errorf("Read file %s failed:", path, err.Error())
		return
	}
	defer file.Close()

	startTime := time.Now()
	stream, err := GetSoberClient().BatchSet(context.Background())
	scanner := bufio.NewScanner(file)
	dataStream := make(chan string, 200)

	fileSize := info.Size()
	count := 100
	cnt := 0
	bar := pb3.Simple.Start(count)
	go func() {
		for i := 0; i < count-1; i++ {
			bar.Increment()
			cnt++
			time.Sleep(time.Duration(fileSize*10/(5*OneM)) * time.Millisecond)
		}
	}()
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for scanner.Scan() {
			dataStream <- scanner.Text()
		}
		close(dataStream)
	}()

	for data := range dataStream {
		err = stream.Send(&pb.StreamRequest{Data: data})
		if err != nil {
			_, err := stream.CloseAndRecv()
			if err != nil {
				Errorf(err.Error())
				return
			}
		}
	}
	wg.Wait()

	resp, err := stream.CloseAndRecv()
	if err != nil {
		Errorf(err.Error())
		return
	}

	//endTime := time.Now()
	bar.Add(count - cnt)
	bar.Finish()
	Print("Record Count: %d", resp.Cnt)
	Print("Elapsed time: %v", time.Since(startTime))
}
