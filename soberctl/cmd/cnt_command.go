package cmd

import (
	"context"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"github.com/spf13/cobra"
)

func NewCntCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "cnt",
		Short: "Counts the number of objects",
		Run:   CntCommandFunc,
	}
	return cmd
}

func CntCommandFunc(cmd *cobra.Command, args []string) {
	request := &pb.SoberRequest{}
	startTime := time.Now()
	resp, err := GetSystemClient().Count(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	p := message.NewPrinter(language.English)
	Print("Total objects %s", p.Sprintf("%d", resp.Cnt))
	Print("Elapsed time: %v", time.Since(startTime))
}
