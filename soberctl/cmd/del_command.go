package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"github.com/spf13/cobra"
)

func NewDeleteCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "del <key> <attributes>",
		Short: "Deletes object attributes",
		Run:   deleteCommandFunc,
	}
	return cmd
}

func deleteCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 2 {
		ExitWithError(ExitBadArgs, fmt.Errorf("del command requires primary key and attributes name as its argument"))
	}

	request := &pb.Request{
		Oid:     args[0],
		Content: args[1],
	}
	startTime := time.Now()
	resp, err := GetSoberClient().Delete(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	Print("%d", resp.Cnt)
	Print("Elapsed time: %v", time.Since(startTime))
}
