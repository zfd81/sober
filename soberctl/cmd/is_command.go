package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"github.com/spf13/cobra"
)

func NewIsCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "is <key> <expression>",
		Short: "Judges whether the object meets the conditions",
		Run:   IsCommandFunc,
	}
	return cmd
}

func IsCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 2 {
		ExitWithError(ExitBadArgs, fmt.Errorf("is command requires primary key and judgment expression as its argument"))
	}

	request := &pb.Request{
		Oid:       args[0],
		Condition: args[1],
	}
	startTime := time.Now()
	resp, err := GetSoberClient().Is(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	Print("%v", resp.Signal)
	Print("Elapsed time: %v", time.Since(startTime))
}
