package cmd

import (
	"context"
	"fmt"

	"github.com/zfd81/rooster/util"

	pb "gitee.com/zfd81/sober/proto/soberpb"

	"github.com/spf13/cobra"
)

const (
	ONE_KB = 1024
	ONE_MB = 1024 * ONE_KB
	ONE_GB = 1024 * ONE_MB
)

func NewMemberCommand() *cobra.Command {
	ac := &cobra.Command{
		Use:   "member <subcommand>",
		Short: "Member related commands",
	}
	ac.AddCommand(newMemberStatusCommand())
	ac.AddCommand(newMemberListCommand())
	return ac
}

func newMemberStatusCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "stat [member id]",
		Short: "View member status",
		Run:   memberStatusCommandFunc,
	}
}

func newMemberListCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "Lists all members",
		Run:   memberListCommandFunc,
	}
}

func memberStatusCommandFunc(cmd *cobra.Command, args []string) {
	//request := &pb.SoberRequest{}
	//if len(args) > 0 {
	//	request.Id = args[0]
	//}
	//resp, err := GetClusterClient().MemberStatus(context.Background(), request)
	//if err != nil {
	//	Errorf(err.Error())
	//	return
	//}

	//p := message.NewPrinter(language.English)
	//
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("ID", 15, " "), "|", util.Rpad(fmt.Sprintf("%s", resp.Id), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("ENDPOINT", 15, " "), "|", util.Rpad(fmt.Sprintf("%s:%d", resp.Address, resp.Port), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("NAMESPACE", 15, " "), "|", util.Rpad(fmt.Sprintf("%s", resp.Namespace), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("SERVICENAME", 15, " "), "|", util.Rpad(fmt.Sprintf("%s", resp.ServiceName), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("START-UP TIME", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", resp.StartUpTime), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35s %1s\n", "|", util.Rpad("ROLE", 15, " "), "|", util.Rpad(fmt.Sprintf("%s", resp.Role), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s  %52s %1s\n", "|", util.Rpad("MEMORY(KB)", 31, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("ALLOC", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.Alloc/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("SYS", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.Sys/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("HEAP ALLOC", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.HeapAlloc/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("HEAP SYS", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.HeapSys/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("HEAP INUSE", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.HeapInuse/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("STACK SYS", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.StackSys/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")
	//fmt.Printf("%1s %15s %1s %35v %1s\n", "|", util.Rpad("STACK INUSE", 15, " "), "|", util.Rpad(fmt.Sprintf("%v", p.Sprintf("%d", resp.MemStats.StackInuse/ONE_KB)), 35, " "), "|")
	//fmt.Println("+-----------------+-------------------------------------+")

}

func memberListCommandFunc(cmd *cobra.Command, args []string) {
	request := &pb.SoberRequest{}
	resp, err := GetClusterClient().ListMembers(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	fmt.Println("+-------+--------------------+-----------+---------------------+----------+")
	fmt.Printf("%1s %5s %1s %18s %1s %9s %1s %19s %1s %8s %1s\n", "|", "NUM ", "|", "ENDPOINT     ", "|", "COMM PORT", "|", "START-UP TIME   ", "|", "ROLE  ", "|")
	fmt.Println("+-------+--------------------+-----------+---------------------+----------+")
	for i, n := range resp.Members {
		fmt.Printf("%1s %5s %1s %18s %1s %9s %1s %19s %1s %8s %1s\n", "|", fmt.Sprintf("%d", i+1), "|", util.Rpad(fmt.Sprintf("%s:%d", n.Address, n.Port), 17, " "), "|", fmt.Sprintf("%d", n.Cport), "|", n.StartUpTime, "|", util.Rpad(n.Role, 8, " "), "|")
	}
	fmt.Println("+-------+--------------------+-----------+---------------------+----------+")
}
