package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"github.com/spf13/cobra"
)

func NewClearCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "clear <key>",
		Short: "Clears object",
		Run:   ClearCommandFunc,
	}
	return cmd
}

func ClearCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		ExitWithError(ExitBadArgs, fmt.Errorf("clear command requires primary key as its argument"))
	}

	request := &pb.Request{
		Oid: args[0],
	}
	startTime := time.Now()
	resp, err := GetSoberClient().Clear(context.Background(), request)
	elapsed := time.Since(startTime)
	if err != nil {
		Errorf(err.Error())
		return
	}
	Print("%d", resp.Cnt)
	Print("Elapsed time: %v", elapsed)
}
