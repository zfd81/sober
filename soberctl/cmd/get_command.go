package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"

	"github.com/spf13/cobra"
)

func NewGetCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "get <key> [expressions...]",
		Short: "Gets object property",
		Run:   GetCommandFunc,
	}
	return cmd
}

func GetCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		ExitWithError(ExitBadArgs, fmt.Errorf("get command requires primary key as its argument"))
	}

	request := &pb.Request{
		Oid: args[0],
	}

	if len(args) > 1 {
		request.Content = args[1]
	}

	startTime := time.Now()
	resp, err := GetSoberClient().Get(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	if len(resp.Data) < 1 {
		Print("%s", "empty")
	} else {
		index := 1
		for k, v := range resp.Data {
			Print("%d) %s = %s", index, k, v)
			index++
		}
	}
	elapsed := time.Since(startTime)
	Print("Elapsed time: %v", elapsed)
}
