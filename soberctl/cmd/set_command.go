package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"

	"github.com/spf13/cobra"
)

func NewSetCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "set <key> <attribute> <value>",
		Short: "Sets object property",
		Run:   SetCommandFunc,
	}
	return cmd
}

func SetCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 3 {
		ExitWithError(ExitBadArgs, fmt.Errorf("set command requires primary key and attribute name and attribute value as its argument"))
	}

	request := &pb.Request{
		Oid: args[0],
		Data: map[string]string{
			args[1]: args[2],
		},
	}
	startTime := time.Now()
	resp, err := GetSoberClient().Set(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	Print("%d", resp.Cnt)
	Print("Elapsed time: %v", time.Since(startTime))
}
