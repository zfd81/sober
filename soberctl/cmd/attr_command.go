package cmd

import (
	"context"
	"fmt"
	"time"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"github.com/spf13/cobra"
)

func NewAttrCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "attr <key>",
		Short: "Lists all Attribute names of the object",
		Run:   AttrCommandFunc,
	}
	return cmd
}

func AttrCommandFunc(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		ExitWithError(ExitBadArgs, fmt.Errorf("attr command requires primary key as its argument"))
	}

	request := &pb.Request{
		Oid: args[0],
	}
	startTime := time.Now()
	resp, err := GetSoberClient().AttributeSet(context.Background(), request)
	if err != nil {
		Errorf(err.Error())
		return
	}
	elapsed := time.Since(startTime)
	if len(resp.Attributes) < 1 {
		Print("%s", "empty")
	} else {
		Print("%s", resp.Attributes)
		Print("Total %d attributes", len(resp.Attributes))
	}
	Print("Elapsed time: %v", elapsed)
}
