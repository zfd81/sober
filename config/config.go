package config

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	Name           string   `mapstructure:"name"`
	Version        string   `mapstructure:"version"`
	Banner         string   `mapstructure:"banner"`
	Protocol       string   `mapstructure:"protocol"`
	Port           uint32   `mapstructure:"port"`
	Cport          uint32   `mapstructure:"cport"`
	Members        []string `mapstructure:"members"`
	BufferSize     int      `mapstructure:"buffer-size"`
	BatchWriteSize int      `mapstructure:"batch-write-size"`
	Log            Log      `mapstructure:"log"`
	Stores         []Store  `mapstructure:"stores"`
}

type Store struct {
	Driver       string        `mapstructure:"driver"`
	Address      string        `mapstructure:"address"`
	User         string        `mapstructure:"user"`
	Password     string        `mapstructure:"pwd"`
	Database     string        `mapstructure:"db"`
	DialTimeout  time.Duration `mapstructure:"dial-timeout"`  //建立新连接的拨号超时时间
	ReadTimeout  time.Duration `mapstructure:"read-timeout"`  //连接读取超时时间
	WriteTimeout time.Duration `mapstructure:"write-timeout"` //连接写入超时时间
}

func (s *Store) Key() string {
	return s.Driver + ":" + s.User + ":" + s.Database
}

type Log struct {
	Level         string        `mapstructure:"level"`
	RotationTime  time.Duration `mapstructure:"rotation-time"`  //日志文件轮换间隔（单位:小时）
	RetentionTime time.Duration `mapstructure:"retention-time"` //日志文件保留时间（单位:小时）
}

const (
	ConfigName = "sober"
	ConfigPath = "."
	ConfigType = "yaml"

	banner_bulbhead = `
	 ___  _____  ____  ____  ____ 
	/ __)(  _  )(  _ \( ___)(  _ \
	\__ \ )(_)(  ) _ < )__)  )   /
	(___/(_____)(____/(____)(_)\_)
	`

	banner_slant = `
               __             
   _________  / /_  ___  _____
  / ___/ __ \/ __ \/ _ \/ ___/
 (__  ) /_/ / /_/ /  __/ /    
/____/\____/_.___/\___/_/        
	`
)

var defaultConf = Config{
	Name:           "Sober",
	Version:        "1.0.0",
	Banner:         banner_bulbhead,
	Protocol:       "tcp",
	Port:           2022,
	Cport:          8143,
	BufferSize:     3,
	BatchWriteSize: 2000,
	Log: Log{
		Level:         "INFO",
		RotationTime:  24,
		RetentionTime: 7 * 24,
	},
}

var globalConf = defaultConf

func init() {
	HOME := os.Getenv("SOBER_HOME") //获取环境变量值
	viper.SetConfigName(ConfigName)
	viper.AddConfigPath(ConfigPath)
	viper.AddConfigPath(HOME)
	viper.SetConfigType(ConfigType)
	if err := viper.ReadInConfig(); err == nil {
		err = viper.Unmarshal(&globalConf)
		if err != nil {
			panic(fmt.Errorf("Fatal error when reading %s config, unable to decode into struct, %v", ConfigName, err))
		}
	}
}

func GetConfig() *Config {
	return &globalConf
}
