#! /bin/bash

#macOS版本
go build -o bin/macos/sober ./main.go
go build -o bin/macos/soberctl ./soberctl/main.go

#Linux版本
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/linux/sober ./main.go
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/linux/soberctl ./soberctl/main.go

#Windows版本
#CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o bin/windows/sober.exe ./main.go
#CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o bin/windows/soberctl.exe ./soberctl/main.go