package server

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"

	pb "gitee.com/zfd81/sober/proto/soberpb"
	"gitee.com/zfd81/sober/store"
)

type SoberSystem struct {
}

func (s SoberSystem) Count(ctx context.Context, request *pb.SoberRequest) (*pb.SoberResponse, error) {
	cnt, err := store.Count()
	if err != nil {
		logrus.Errorf("Error counting objects: %s", err.Error())
		return nil, err
	}

	logrus.WithField("cnt", cnt).Infof("Count of objects")

	return &pb.SoberResponse{
		Cnt:       cnt,
		Timestamp: time.Now().Unix(),
	}, nil
}
