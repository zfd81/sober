package server

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"gitee.com/zfd81/sober/expression/functions"

	"github.com/sirupsen/logrus"

	"gitee.com/zfd81/sober/expression"

	"github.com/zfd81/rooster/types/container"

	"github.com/spf13/cast"

	"gitee.com/zfd81/sober/store"

	pb "gitee.com/zfd81/sober/proto/soberpb"
)

func ms2mi(data map[string]string) map[string]interface{} {
	var d = map[string]interface{}{}
	for k, v := range data {
		d[k] = v
	}
	return d
}

func mi2ms(data map[string]interface{}) map[string]string {
	var d = map[string]string{}
	for k, v := range data {
		d[k] = cast.ToString(v)
	}
	return d
}

type Sober struct{}

func (s Sober) Set(ctx context.Context, request *pb.Request) (*pb.Response, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	data := request.GetData()
	if data == nil || len(data) < 1 {
		return nil, fmt.Errorf("Attributes value cannot be empty")
	}

	//构建日志信息
	fields := logrus.Fields{
		"ObjectID": oid,
	}
	for k, v := range data {
		fields[k] = v
	}

	cnt, err := store.Set(request.Oid, ms2mi(data))
	if err != nil {
		logrus.WithFields(fields).Errorf("Error setting object attributes: %s", err.Error())
		return nil, err
	}

	logrus.WithFields(fields).Infof("Set object attributes")

	return &pb.Response{
		Cnt:       cnt,
		Timestamp: time.Now().Unix(),
	}, nil
}

func writer(fleet *store.Fleet, index string) {
	stream := fleet.GetBoat(index).Channel()
	for data := range stream {
		err := store.BatchSet(index, data)
		if err != nil {
			fleet.SetError(err)
			return
		}
	}
}

func (s Sober) BatchSet(stream pb.Sober_BatchSetServer) error {
	fleet := store.NewFleet()
	for _, i := range store.PoolIndexs() {
		index := i
		fleet.SetBoat(index, store.NewBoat())
		go writer(fleet, index)
	}
	var r *pb.StreamRequest
	var err error
	flag := false
	var attributes []string
	attrlen := 0
	var cnt int64 = 0
	for {
		r, err = stream.Recv()
		if err == io.EOF {
			fleet.Clear()
			break
		}
		if err != nil {
			fleet.Clear()
			return err
		}
		if fleet.Disabled() {
			break
		}
		line := strings.TrimSpace(r.Data)
		if line != "" {
			if flag {
				row := strings.Split(line, ",")
				if attrlen <= len(row) {
					id := row[0]
					index := store.StorageIndex(id)
					obj := store.NewObject(id)
					for i := 1; i < attrlen; i++ {
						obj.Set(attributes[i], row[i])
					}
					fleet.GetBoat(index).Add(obj)
					cnt++
				} else {
					return fmt.Errorf("Data format error near line %d", cnt)
				}
			} else {
				attributes = strings.Split(line, ",")
				attrlen = len(attributes)
				flag = true
			}
		}
	}
	err = fleet.Wait()
	if err != nil {
		return err
	}

	logrus.WithField("cnt", cnt).Infof("Batch set object attributes")

	return stream.SendAndClose(&pb.Response{
		Cnt:       cnt,
		Timestamp: time.Now().Unix(),
	})
}

func (s Sober) Delete(ctx context.Context, request *pb.Request) (*pb.Response, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	attributes := strings.TrimSpace(request.Content)
	if attributes == "" {
		return nil, fmt.Errorf("Attribute name cannot be empty")
	}
	attrArry := strings.Split(attributes, ",")

	//构建日志信息
	fields := logrus.Fields{
		"ObjectID":   oid,
		"Attributes": attributes,
	}

	cnt, err := store.Delete(oid, attrArry...)
	if err != nil {
		logrus.WithFields(fields).Errorf("Error deleting object attributes: %s", err.Error())
		return nil, err
	}

	//打印日志
	logrus.WithFields(fields).Infof("Delete object attributes")

	return &pb.Response{
		Cnt:       cnt,
		Timestamp: time.Now().Unix(),
	}, err
}

func (s Sober) Clear(ctx context.Context, request *pb.Request) (*pb.Response, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	cnt, err := store.Clear(oid)
	if err != nil {
		logrus.WithField("ObjectID", oid).Errorf("Error emptying object: %s", err.Error())
		return nil, err
	}

	//打印日志
	logrus.WithField("ObjectID", oid).Infof("Empty object")

	return &pb.Response{
		Cnt:       cnt,
		Timestamp: time.Now().Unix(),
	}, nil
}

func (s Sober) Get(ctx context.Context, request *pb.Request) (*pb.QueryResponse, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	set := container.HashSet{}
	exprs := strings.TrimSpace(request.GetContent())
	expressions := expression.Exprs(exprs)
	for _, expr := range expressions {
		fields, err := expression.Fields(expr)
		if err != nil {
			return nil, fmt.Errorf("syntax error:", err)
		}
		for _, field := range fields {
			set.Add(field)
		}
	}
	attributes := make([]string, set.Size())
	set.Iterator(func(i int, v interface{}) {
		attributes[i] = cast.ToString(v)
	})

	//构建日志信息
	fields := logrus.Fields{
		"ObjectID": oid,
		"Content":  functions.If(exprs == "", "ALL", exprs),
	}

	data, err := store.Get(oid, attributes...)
	if err != nil {
		logrus.WithFields(fields).Errorf("Error getting object info: %s", err.Error())
		return nil, err
	}

	if len(attributes) == 0 {
		logrus.WithFields(fields).Infof("Get object information")
		return &pb.QueryResponse{
			Data:      mi2ms(data),
			Timestamp: time.Now().Unix(),
		}, nil
	} else {
		result := map[string]string{}
		for _, expr := range expressions {
			v, err := expression.Eval(expr, expression.ConvertDataTypes(data))
			if err != nil {
				logrus.WithFields(fields).Errorf("Error getting object info: %s", err.Error())
				return nil, err
			}
			result[expr] = cast.ToString(v)
		}
		logrus.WithFields(fields).Infof("Get object information")
		return &pb.QueryResponse{
			Data:      result,
			Timestamp: time.Now().Unix(),
		}, nil
	}
}

func (s Sober) Is(ctx context.Context, request *pb.Request) (*pb.JudgmentResponse, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	expr := strings.TrimSpace(request.Condition)
	if expr == "" {
		return nil, fmt.Errorf("Condition expression cannot be empty")
	}

	//构建日志信息
	fs := logrus.Fields{
		"ObjectID":  oid,
		"Condition": expr,
	}

	fields, err := expression.Fields(expr)
	if err != nil {
		return nil, fmt.Errorf("syntax error:", err)
	}

	data, err := store.Get(oid, fields...)
	if err != nil {
		logrus.WithFields(fs).Errorf("Error in judging objects: %s", err.Error())
		return nil, err
	}

	v, err := expression.Eval(expr, expression.ConvertDataTypes(data))
	if err != nil {
		logrus.WithFields(fs).Errorf("Error in judging objects: %s", err.Error())
		return nil, err
	}

	val, ok := v.(bool)
	if !ok {
		return nil, fmt.Errorf("Expression type error, not a condition")
	}

	logrus.WithFields(fs).Infof("Judgment object")

	return &pb.JudgmentResponse{
		Signal:    val,
		Timestamp: time.Now().Unix(),
	}, nil
}

func (s Sober) AttributeSet(ctx context.Context, request *pb.Request) (*pb.Response, error) {
	oid := strings.TrimSpace(request.Oid)
	if oid == "" {
		return nil, fmt.Errorf("object ID cannot be empty")
	}

	attributes, err := store.AttrSet(oid)
	if err != nil {
		logrus.WithField("ObjectID", oid).Errorf("Error getting object attribute set: %s", err.Error())
		return nil, err
	}

	logrus.WithField("ObjectID", oid).Infof("Get object attribute set")

	return &pb.Response{
		Attributes: attributes,
		Timestamp:  time.Now().Unix(),
	}, nil
}
