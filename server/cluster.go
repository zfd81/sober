package server

import (
	"context"
	"time"

	"github.com/spf13/cast"

	"gitee.com/zfd81/sober/cluster"

	pb "gitee.com/zfd81/sober/proto/soberpb"
)

type Cluster struct{}

func (c *Cluster) ListMembers(ctx context.Context, request *pb.SoberRequest) (*pb.Members, error) {
	members := []*pb.Member{}
	for _, n := range cluster.Members() {
		member := &pb.Member{
			Address:     n.Addr.String(),
			Port:        n.ServPort,
			Cport:       cast.ToUint32(n.Port),
			ServiceName: n.Name,
			StartUpTime: time.Unix(n.StartUpTime, 0).Format("2006-01-02 15:04:05"),
		}
		if n.FullAddress() == cluster.GetLeaderNode().FullAddress() {
			member.Role = "Leader"
		} else {
			member.Role = "Follower"
		}
		members = append(members, member)
	}
	return &pb.Members{
		Members: members,
	}, nil
}

func (c *Cluster) MemberStatus(ctx context.Context, request *pb.SoberRequest) (*pb.Member, error) {
	node := cluster.CurrentNode()
	member := &pb.Member{
		Address:     node.Addr.String(),
		Port:        node.ServPort,
		Cport:       cast.ToUint32(node.Port),
		ServiceName: node.Name,
		StartUpTime: time.Unix(node.StartUpTime, 0).Format("2006-01-02 15:04:05"),
	}
	ms := node.MemStats()
	member.MemStats = &pb.MemoryStats{
		Alloc:        ms.Alloc,
		TotalAlloc:   ms.TotalAlloc,
		Sys:          ms.Sys,
		Mallocs:      ms.Mallocs,
		Frees:        ms.Frees,
		HeapAlloc:    ms.HeapAlloc,
		HeapSys:      ms.HeapSys,
		HeapIdle:     ms.HeapIdle,
		HeapInuse:    ms.HeapInuse,
		HeapReleased: ms.HeapReleased,
		HeapObjects:  ms.HeapObjects,
		StackInuse:   ms.StackInuse,
		StackSys:     ms.StackSys,
	}
	if cluster.IsLeader() {
		member.Role = "Leader"
	} else {
		member.Role = "Follower"
	}
	return member, nil
}
