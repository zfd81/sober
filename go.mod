module gitee.com/zfd81/sober

go 1.16

require (
	github.com/antonmedv/expr v1.9.0
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/fatih/color v1.13.0
	github.com/go-redis/redis/v8 v8.11.4
	github.com/golang/protobuf v1.5.2
	github.com/hashicorp/memberlist v0.3.0
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cast v1.4.1
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	github.com/zfd81/rooster v0.0.0-20220612032356-4456fdaa1ec9
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.7
	google.golang.org/grpc v1.43.0
	stathat.com/c/consistent v1.0.0
)
