package log

import (
	"time"

	"gitee.com/zfd81/sober/config"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

func NewFileHook(path string) (logrus.Hook, error) {
	writer, err := rotatelogs.New(
		path+".%Y%m%d%H",
		rotatelogs.WithLinkName(path),
		rotatelogs.WithMaxAge(config.GetConfig().Log.RetentionTime*time.Hour),
		rotatelogs.WithRotationTime(config.GetConfig().Log.RotationTime*time.Hour),
	)
	if err != nil {
		return nil, err
	}
	return lfshook.NewHook(
		lfshook.WriterMap{
			logrus.DebugLevel: writer,
			logrus.InfoLevel:  writer,
			logrus.WarnLevel:  writer,
			logrus.ErrorLevel: writer,
			logrus.FatalLevel: writer,
			logrus.PanicLevel: writer,
		},
		&logrus.TextFormatter{
			FullTimestamp:   true,
			TimestampFormat: "2006-01-02 15:04:05.999",
			DisableColors:   true},
	), nil
}
