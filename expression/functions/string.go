package functions

import (
	"encoding/json"
	"fmt"
	"strings"
)

func Left(str string, length int) string {
	if str == "" || length < 0 {
		return ""
	}
	strRune := []rune(str)
	if length < len(strRune) {
		return string(strRune[:length])
	} else {
		return str
	}
}

func Right(str string, length int) string {
	if str == "" || length < 0 {
		return ""
	}
	strRune := []rune(str)
	strLen := len(strRune)
	if length < strLen {
		return string(strRune[strLen-length:])
	} else {
		return str
	}
}

func Substr(str string, position int, length int) string {
	strRune := []rune(str)
	if position > len(strRune) {
		return ""
	} else if position > 0 {
		return Left(string(strRune[position-1:]), length)
	} else if (0 - position) <= len(strRune) {
		return Left(string(strRune[position+len(strRune):]), length)
	}
	return ""
}

func Split(str, sep string) []string {
	return strings.Split(str, sep)
}

func SplitN(str, sep string, n int) []string {
	return strings.SplitN(str, sep, n)
}

func Object(str string) map[string]interface{} {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(str), &m)
	if err != nil {
		throwException("JSON format error")
	}
	return m
}

func throwException(format string, msgs ...interface{}) {
	panic(fmt.Sprintf(format, msgs...))
}
