package functions

import (
	"regexp"

	"github.com/spf13/cast"
)

func isInt(str string) bool {
	return regexp.MustCompile(`^\d+$`).MatchString(str)
}

func isFloat(str string) bool {
	return regexp.MustCompile(`^\d+[.]\d+$`).MatchString(str)
}

func Number(val interface{}) interface{} {
	switch val.(type) {
	case string:
		if isInt(cast.ToString(val)) {
			return cast.ToInt(val)
		} else if isFloat(cast.ToString(val)) {
			return cast.ToFloat64(val)
		} else if cast.ToString(val) == "true" {
			return 1
		} else if cast.ToString(val) == "false" {
			return 0
		} else {
			throwException("Number format error")
		}
	case int8, int16, int32, int64, int, float32, float64:
		return val
	case bool:
		if cast.ToBool(val) {
			return 1
		} else {
			return 0
		}
	default:
		throwException("Number format error")
	}
	return 0
}
