package expression

import (
	"regexp"
	"strings"

	"github.com/antonmedv/expr/ast"
	"github.com/antonmedv/expr/parser"

	"github.com/zfd81/rooster/types/container"

	"github.com/spf13/cast"

	"gitee.com/zfd81/sober/expression/functions"
	"github.com/antonmedv/expr"
)

var (
//keywords = map[string]bool{
//	"if":     true,
//	"else":   true,
//	"decode": true,
//	"eq":     true,
//	"left":   true,
//	"right":  true,
//	"substr": true,
//	"obj":    true,
//}
)

type visitor struct {
	identifiers []string
}

func (v *visitor) Enter(node *ast.Node) {}
func (v *visitor) Exit(node *ast.Node) {
	if n, ok := (*node).(*ast.IdentifierNode); ok {
		v.identifiers = append(v.identifiers, n.Value)
	}
}

func isInt(str string) bool {
	return regexp.MustCompile(`^\d+$`).MatchString(str)
}

func isFloat(str string) bool {
	return regexp.MustCompile(`^\d+[.]\d+$`).MatchString(str)
}

func isBool(str string) bool {
	return str == "true" || str == "false"
}

func Exprs(str string) []string {
	exprs := []string{}
	stack := container.NewArrayStack()
	strLen := len(str) //字符串长度
	start := 0         //替换内容的开始位置
	for i := 0; i < strLen; i++ {
		char := str[i]
		if char == ',' {
			if stack.Empty() {
				s := str[start:i]
				if strings.TrimSpace(s) != "" {
					exprs = append(exprs, s)
				}
				start = i + 1
			}
		} else {
			if char == '\'' || char == '"' {
				c, _ := stack.Peek()
				if c == char {
					stack.Pop()
				} else {
					stack.Push(char)
				}
			} else if char == '(' || char == '[' || char == '{' {
				c, _ := stack.Peek()
				if cast.ToUint8(c) != '\'' && cast.ToUint8(c) != '"' {
					stack.Push(char)
				}
			} else if char == ')' {
				c, _ := stack.Peek()
				if cast.ToUint8(c) == '(' {
					stack.Pop()
				}
			} else if char == ']' {
				c, _ := stack.Peek()
				if cast.ToUint8(c) == '[' {
					stack.Pop()
				}
			} else if char == '}' {
				c, _ := stack.Peek()
				if cast.ToUint8(c) == '{' {
					stack.Pop()
				}
			}
		}
	}
	if start < strLen {
		s := str[start:strLen]
		if strings.TrimSpace(s) != "" {
			exprs = append(exprs, s)
		}
	}
	return exprs
}

//func Fields(str string) []string {
//	fields := util.Fields(str)
//	newfields := make([]string, 0)
//	for i, f := range fields {
//		if keywords[f] {
//			continue
//		}
//		newfields = append(newfields, fields[i])
//	}
//	return newfields
//}

func Fields(str string) ([]string, error) {
	tree, err := parser.Parse(str)
	if err != nil {
		return nil, err
	}
	visitor := &visitor{}
	ast.Walk(&tree.Node, visitor)
	return visitor.identifiers, nil
}

func ConvertDataTypes(env map[string]interface{}) map[string]interface{} {
	for k, v := range env {
		if isInt(cast.ToString(v)) {
			env[k] = cast.ToInt(v)
		} else if isFloat(cast.ToString(v)) {
			env[k] = cast.ToFloat64(v)
		} else if isBool(cast.ToString(v)) {
			env[k] = cast.ToBool(v)
		}
	}
	return env
}

func Eval(script string, env map[string]interface{}) (interface{}, error) {
	env["if"] = functions.If
	env["decode"] = functions.Decode
	env["eq"] = functions.Equality
	env["left"] = functions.Left
	env["right"] = functions.Right
	env["substr"] = functions.Substr
	env["obj"] = functions.Object
	env["num"] = functions.Number
	env["split"] = functions.Split
	env["splitn"] = functions.SplitN
	env["time"] = functions.Time

	env["AddDays"] = functions.AddDays
	env["DiffDay"] = functions.DiffDay

	options := []expr.Option{
		expr.Env(env),
		expr.Operator("+", "AddDays"),
		expr.Operator("-", "DiffDay"),
	}

	program, err := expr.Compile(script, options...)
	if err != nil {
		return "", err
	}
	return expr.Run(program, env)
}
