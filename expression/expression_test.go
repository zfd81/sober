package expression

import (
	"encoding/json"
	"fmt"
	"regexp"
	"testing"

	"gitee.com/zfd81/sober/expression/functions"
)

func TestEval(t *testing.T) {
	env := map[string]interface{}{
		"greet":   "Hello, %v!",
		"age":     20,
		"names":   []string{"world", "you"},
		"sprintf": fmt.Sprintf,
	}
	stmt := `
35.7+8.99
`
	v, err := Eval(stmt, env)
	if err != nil {
		t.Log(err)
	}
	t.Log(v)
}

var numPattern = regexp.MustCompile(`^\d+.?(\d+)$`)

/**
判断字符串是否为 纯数字 包涵浮点型
*/
func IsNumber(s string) bool {
	return numPattern.MatchString(s)
}
func TestFields(t *testing.T) {
	expr := `
		'aa'+"'bb'")*left + ff + if9
	`
	t.Log(Fields(expr))
	t.Log(functions.Decode(1, 2))
	t.Log(functions.Decode(1, 1, 2))
	t.Log(functions.Decode(2, 1, 2, 3))
	t.Log(functions.Decode(2, 1, 2, 3, 5, 6))
	t.Log(functions.If(1 > 2, 23, functions.If(2 > 30, 11, 22)))
	fff := map[string]interface{}{
		"name": "zfd",
		"age":  21,
	}

	tt, _ := json.Marshal(fff)
	t.Log(string(tt))
	m := map[string]interface{}{
		"fff": "ddd",
	}
	val, err := Eval("in(1>2,22,33)", m)
	if err != nil {
		t.Error(err)
	}
	t.Log(val)
}

func TestExprs(t *testing.T) {
	str := "'a]aa,bbb,in(11,22)',,',af{33,44,"
	ss := Exprs(str)
	t.Log(ss)
	t.Log(len(ss))
}
